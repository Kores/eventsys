# EventSys

EventSys is an event system designed for Event-Driven Development and Event-oriented applications.

## Simple event

A simple event can be defined as following:

```kotlin
interface MessageEvent {
    val message: String
}
```

## Simple factory

In order to create event instances, you need a factory:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}
```

## Factory and Event generation

The factory interface by itself is not instantiable, so to get an instance of the factory you need to use the factory generator:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}

class ReadmeTest: FunSpec({
    test("readme") {
        val eventManager = DefaultEventManager()
        val factory = eventManager.eventGenerator.createFactory<EventFactory>().resolve()
        val message = factory.createMessageEvent("Hello world!")
        message.message.shouldBe("Hello world!")
    }
})
```

## Dispatch events

Now that we have created the event, we can dispatch it:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}

class ReadmeTest: FunSpec({
    test("readme") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        val message = factory.createMessageEvent("Hello world!")
        
        val result = manager.dispatch(message, this)
    }
})
```

## Listen to events

We have dispatched the event, but there is no listener registered to handle them, so let register a simple method listener:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}

class MessageListener {
    @Listener
    fun onMessage(message: MessageEvent): String {
        return message.message
    }
}

class ReadmeTest : FunSpec({
    test("readme") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MessageListener())

        val message = factory.createMessageEvent("Hello world!")
        message.message.shouldBe("Hello world!")

        val result = manager.dispatch(message, this)

        result.listenExecutionResults.first().await()
            .result.shouldBe(ListenResult.Value("Hello world!"))
    }
})
```

### suspend

If your listener needs to use `suspend` functions, just annotated it with `suspend`:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}

class MessageListener {
    @Listener
    suspend fun onMessage(message: MessageEvent): String {
        return message.message
    }
}

class ReadmeTest : FunSpec({
    test("readme") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MessageListener())

        val message = factory.createMessageEvent("Hello world!")
        message.message.shouldBe("Hello world!")

        val result = manager.dispatch(message, this)

        result.listenExecutionResults.first().await()
            .result.shouldBe(ListenResult.Value("Hello world!"))
    }
})
```

## Read more

Read more at the [documentation](https://idocs.xyz/)