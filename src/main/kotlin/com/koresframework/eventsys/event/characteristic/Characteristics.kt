/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.event.characteristic

import com.koresframework.eventsys.event.Event
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentHashMapOf

interface Characteristics {

    val allCharacteristics: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>

    /**
     * Gets a stored characteristic, returns `null` is there is no characteristic stored.
     */
    fun <C: CharacteristicKey<V>, V> get(key: C): CharacteristicValue<C, V>?

    /**
     * Creates a new [Characteristics] object with the provided characteristic [C].
     */
    fun <C: CharacteristicKey<V>, V> add(characteristic: C, value: CharacteristicValue<C, V>): Characteristics

    /**
     * Creates a new [Characteristics] object with all characteristics provided in [map].
     */
    fun add(map: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>): Characteristics

    /**
     * Session which accepts multiple characteristics to be set at once to a given event [H].
     */
    interface With<H> where H: CharacteristicHolder<H>, H: Event {
        /**
         * Adds the characteristic [C] to the current session, mutating the session.
         */
        fun <C: CharacteristicKey<V>, V> characteristic(characteristic: C, value: CharacteristicValue<C, V>): With<H>

        /**
         * Builds the event with all characteristics set in this session.
         */
        fun build(): H
    }

    class WithBuilder<H>(val holder: H) : With<H> where H: CharacteristicHolder<H>, H: Event  {
        private val mutableMap = mutableMapOf<CharacteristicKey<*>, CharacteristicValue<*, *>>()

        override fun <C : CharacteristicKey<V>, V> characteristic(
            characteristic: C,
            value: CharacteristicValue<C, V>
        ): With<H> {
            this.mutableMap[characteristic] = value
            return this
        }

        override fun build(): H =
            this.holder.characteristics(this.mutableMap)
    }

    class Persistent(
        private val characteristics: PersistentMap<CharacteristicKey<*>, CharacteristicValue<*, *>> = persistentHashMapOf()
    ) : Characteristics {

        override val allCharacteristics: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>
            get() = this.characteristics

        @Suppress("UNCHECKED_CAST")
        override fun <C : CharacteristicKey<V>, V> get(key: C): CharacteristicValue<C, V>? {
            val value = this.characteristics[key] ?: return null
            return value as CharacteristicValue<C, V>
        }

        override fun <C : CharacteristicKey<V>, V> add(
            characteristic: C,
            value: CharacteristicValue<C, V>
        ): Characteristics = Persistent(
            this.characteristics.put(characteristic, value)
        )

        override fun add(map: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>): Characteristics =
            Persistent(
                this.characteristics.putAll(map)
            )
    }
}