/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.event.characteristic

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension

/**
 * Interface implemented by events that holds characteristics.
 *
 * The concrete implementation of `characteristics` is done by [CharacteristicExt] class, through
 * the extension mechanism.
 */
@Extension(implement = CharacteristicHolder::class, extensionClass = CharacteristicExt::class)
interface CharacteristicHolder<H> where H: CharacteristicHolder<H>, H: Event {
    /**
     * The characteristics this event holds.
     *
     * To add new characteristics use [characteristic], [characteristics] or [withCharacteristics] functions,
     * trying to add new characteristics through the [`characteristics`][Characteristics] property
     * will not work, since the returned value is immutable/persistent.
     */
    val characteristics: Characteristics

    /**
     * Produces a new event with the provided characteristic [C].
     *
     * To add multiple characteristics at the same time without producing various intermediate events, use
     * [withCharacteristics] or [characteristics] function.
     */
    fun <C: CharacteristicKey<V>, V> characteristic(characteristic: C, value: CharacteristicValue<C, V>): H

    /**
     * Produces a new event with all current characteristics plus characteristics provided in the [map].
     *
     * This is a bulk operation, this means that no intermediate events are created, instead all characteristics
     * are set at once.
     */
    fun characteristics(map: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>): H

    /**
     * Creates a characteristic builder to add multiple characteristics at once.
     */
    fun withCharacteristics(): Characteristics.With<H>
}