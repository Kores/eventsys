/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.event.characteristic

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.DefaultValue

/**
 * Implementation of [CharacteristicHolder] through extension mechanism.
 *
 *
 * ## `EventType` type variable
 *
 * This class uses [EventType] as the **type variable** to denote the original event type. This **type variable** is only used
 * by the implementation to have access to both [Event] and [CharacteristicHolder] methods, as they will not be accessible
 * if the plain type is used.
 *
 * This kind of naming is very uncommon in Java, and the intention is to avoid **Extension Type Variable Matching**, which
 * is a feature that allow event extensions o implement some sort of “reification”.
 *
 * Since this event extension only implements functions provided by [CharacteristicHolder], applying reification will simply
 * cause the methods to not override the original ones, and instead, produce new ones, as method bridging does not work
 * for event extensions.
 *
 * Read more about event extensions in the book to understand this concept.
 */
class CharacteristicExt<EventType>(val event: EventType) where EventType: CharacteristicHolder<EventType>, EventType: Event {

    @get:DefaultValue
    val characteristics: Characteristics = Characteristics.Persistent()

    fun <C: CharacteristicKey<V>, V> characteristic(characteristic: C, value: CharacteristicValue<C, V>): EventType {
        return event.copy(mapOf(
            "characteristics" to event.characteristics.add(characteristic, value)
        )) as EventType
    }

    fun characteristics(map: Map<CharacteristicKey<*>, CharacteristicValue<*, *>>): EventType {
        return event.copy(mapOf(
            "characteristics" to event.characteristics.add(map)
        )) as EventType
    }

    fun withCharacteristics(): Characteristics.With<EventType> =
        Characteristics.WithBuilder(this.event)
}