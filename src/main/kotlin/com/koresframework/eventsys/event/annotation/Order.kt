/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.event.annotation

import com.koresframework.eventsys.extension.UNSORTED
import kotlin.reflect.KClass

/**
 * Defines ordering of Extension Classes, useful when an extension defines a [property default value][DefaultValue],
 * this allows other extensions to access those values without facing *property-not-initialized problems*.
 *
 * There are some types of ordering:
 *
 * #### Constant Ordering
 * Order based on [const] property, lower values come first, higher comes last. Setting it to [integer minimum value][Integer.MIN_VALUE]
 * sets the ordering behavior to **unsorted**, unless other type of ordering is set.
 *
 * #### Extension Type Ordering
 * Allow to set an order [before] or [after] an extension is created, providing more than one extension
 * causes the extension to be created [before] or [after] all those defined extensions.
 *
 * #### Extension-Id Ordering
 * Allow to set an order [before][beforeId] or [after][afterId] an extension is created,
 * but by using id expression instead of class references.
 *
 * Providing more than one extension
 * causes the extension to be created [before][beforeId] or [after][afterId] all those defined extensions.
 *
 * This is useful when the extension is not available in the classpath, but is known.
 *
 * ## Read More
 *
 * - [Id Expression][com.koresframework.eventsys.expr.IdExpression]
 *
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS,
    AnnotationTarget.CONSTRUCTOR,
    AnnotationTarget.FILE,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.LOCAL_VARIABLE,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.TYPE,
    AnnotationTarget.TYPE_PARAMETER,
    AnnotationTarget.VALUE_PARAMETER)
annotation class Order(
    val const: Int = UNSORTED,
    val after: Array<KClass<*>> = [],
    val before: Array<KClass<*>> = [],
    val afterId: Array<String> = [],
    val beforeId: Array<String> = [],
)

