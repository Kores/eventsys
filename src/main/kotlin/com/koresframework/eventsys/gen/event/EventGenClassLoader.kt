/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.gen.event

import com.koresframework.eventsys.context.CLASS_LOADER
import com.koresframework.eventsys.context.EnvironmentContext
import com.koresframework.kores.base.TypeDeclaration
import com.koresframework.kores.bytecode.classloader.CodeClassLoader
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.gen.GeneratedEventClass
import com.koresframework.eventsys.gen.GenerationEnvironment
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

/**
 * [ClassLoader] of all event generated classes
 */
internal object EventGenClassLoader {

    private val globalClassLoader = CodeClassLoader(EventGenClassLoader::class.java.classLoader)
    private val loadedClasses_ = CopyOnWriteArrayList<GeneratedEventClass<*>>()
    val loadedClasses = Collections.unmodifiableList(loadedClasses_)

    fun defineClass(decl: TypeDeclaration, byteArray: ByteArray, disassembled: Lazy<String>): GeneratedEventClass<*> {
        val cl = Event::class.java.classLoader
        val loader = (cl as? CodeClassLoader) ?: globalClassLoader
        return this.defineClass(decl, byteArray, disassembled, loader)
    }

    fun defineClass(decl: TypeDeclaration,
                    byteArray: ByteArray,
                    disassembled: Lazy<String>,
                    ctx: GenerationEnvironment
    ): GeneratedEventClass<*> {
        val loader = ctx.classLoader ?: globalClassLoader
        return this.defineClass(decl, byteArray, disassembled, loader)
    }

    fun defineClass(decl: TypeDeclaration,
                    byteArray: ByteArray,
                    disassembled: Lazy<String>,
                    envCtx: EnvironmentContext,
                    ctx: GenerationEnvironment
    ): GeneratedEventClass<*> {
        val loader = CLASS_LOADER.getOrNull(envCtx.data) ?: ctx.classLoader ?: globalClassLoader
        return this.defineClass(decl, byteArray, disassembled, loader)
    }

    private fun defineClass(decl: TypeDeclaration,
                    byteArray: ByteArray,
                    disassembled: Lazy<String>,
                    classLoader: ClassLoader): GeneratedEventClass<*> {

        val definedClass = try {
            if (classLoader is CodeClassLoader) {
                classLoader.define(decl, byteArray)
            } else {
                this.inject(classLoader, decl.canonicalName, byteArray)
            }
        } catch (e: Exception) {
            CodeClassLoader(classLoader).define(decl, byteArray)
        }

        val sandstoneClass = GeneratedEventClass(definedClass, byteArray, disassembled)

        this.loadedClasses_ += sandstoneClass

        return sandstoneClass
    }

    private fun inject(classLoader: ClassLoader, name: String, bytes: ByteArray): Class<*> {
        val method = ClassLoader::class.java.getDeclaredMethod("defineClass",
                String::class.java, ByteArray::class.java, Int::class.javaPrimitiveType, Int::class.javaPrimitiveType)
        method.isAccessible = true
        try {
            return method.invoke(classLoader, name, bytes, 0, bytes.size) as Class<*>
        } catch (t: Throwable) {
            throw IllegalArgumentException("Cannot inject provided class: $name.", t)
        }
    }

}