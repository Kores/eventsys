/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.impl

import com.koresframework.eventsys.event.EventListener
import com.koresframework.eventsys.event.EventPriority

class EventListenerContainerComparator(val other: Comparator<EventListener<*>>) : Comparator<EventListenerContainer<*>> {
    override fun compare(f: EventListenerContainer<*>, s: EventListenerContainer<*>): Int =
        Comparator.comparing<EventListenerContainer<*>, EventListener<*>>({ it.eventListener }, other)
            .thenComparing { o1, o2 -> o1.isSuperTypeOf(o2.eventType).compareTo(o2.isSuperTypeOf(o1.eventType)) }
            .thenComparing { o1, o2 -> o1.hashCode().compareTo(o2.hashCode()) }
            .thenComparing { o1, o2 -> if (o1.equals(o2)) 0 else -1 }
            .compare(f, s)
}