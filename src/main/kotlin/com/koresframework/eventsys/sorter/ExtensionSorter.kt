/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.sorter

import com.koresframework.eventsys.extension.ExtensionSpecification
import com.koresframework.eventsys.extension.UNSORTED
import com.koresframework.kores.type.`is`

object ExtensionSorter : Comparator<ExtensionSpecification> {
    override fun compare(left: ExtensionSpecification, right: ExtensionSpecification): Int {
        // keep comparison order
        val leftConstUnsorted = left.order.const == UNSORTED
        // keep comparison order
        val rightConstUnsorted = right.order.const == UNSORTED

        val comparators = mutableListOf<Comparator<ExtensionSpecification>>()

        if (!leftConstUnsorted && !rightConstUnsorted)
            comparators.add(Comparator.comparing { it.order.const })

        if (left.order.after.isNotEmpty() || right.order.after.isNotEmpty()) {
            comparators.add(Comparator { o1, o2 -> o1.after(o2) })
        }

        if (left.order.afterId.isNotEmpty() || right.order.afterId.isNotEmpty()) {
            comparators.add(Comparator { o1, o2 -> o1.afterId(o2) })
        }

        if (left.order.before.isNotEmpty() || right.order.before.isNotEmpty()) {
            comparators.add(Comparator { o1, o2 -> o1.before(o2) })
        }

        if (left.order.beforeId.isNotEmpty() || right.order.beforeId.isNotEmpty()) {
            comparators.add(Comparator { o1, o2 -> o1.beforeId(o2) })
        }

        if (comparators.isEmpty())
            return -1;

        val comp = comparators.reduce { acc, comparator ->
            acc.thenComparing(comparator)
        }

        val r = comp.compare(left, right)

        if (r == 0)
            return -1;

        return r
    }

    private fun ExtensionSpecification.after(type: ExtensionSpecification): Int =
        if (type.extensionClass != null && this.order.after.any { it.`is`(type.extensionClass) }) 1
        else if (this.extensionClass != null && type.order.after.any { it.`is`(this.extensionClass) }) -1
        else 0

    private fun ExtensionSpecification.before(type: ExtensionSpecification): Int =
        if (type.extensionClass != null && this.order.before.any { it.`is`(type.extensionClass) }) -1
        else if (this.extensionClass != null && type.order.before.any { it.`is`(this.extensionClass) }) 1
        else 0

    private fun ExtensionSpecification.afterId(type: ExtensionSpecification): Int =
        if (type.extensionClass != null && this.order.afterId.any { it.match(type.extensionClass) }) 1
        else if (this.extensionClass != null && type.order.afterId.any { it.match(this.extensionClass) }) -1
        else 0

    private fun ExtensionSpecification.beforeId(type: ExtensionSpecification): Int =
        if (type.extensionClass != null && this.order.beforeId.any { it.match(type.extensionClass) }) -1
        else if (this.extensionClass != null && type.order.beforeId.any { it.match(this.extensionClass) }) 1
        else 0

}