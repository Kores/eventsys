/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.expr

import com.koresframework.kores.type.canonicalName
import com.koresframework.kores.type.javaSpecName
import com.koresframework.kores.type.simpleName
import java.lang.IllegalStateException
import java.lang.reflect.Type

/**
 * ## Id Expression
 *
 * ### Regex (find)
 *
 * Finds the pattern in the Java Spec Name.
 *
 * #### Expression
 *
 * `@regexFind:EXPRESSION`
 *
 * #### Example
 *
 * `@regexFind:(Super)?PowerExtension`
 *
 * ### Regex (match)
 *
 * Java Spec Name matches the pattern (entirely)
 *
 * #### Expression
 *
 * `@regexMatch:EXPRESSION`
 *
 * #### Example
 *
 * `@regexMatch:Lfoo\/bar\/(Super)?PowerExtension;`
 *
 * ### SimpleName
 *
 * #### Expression
 *
 * `@simpleName:NAME`
 *
 * #### Example
 *
 * `@simpleName:SuperPowerExtension`
 *
 * ### CanonicalName
 *
 * #### Expression
 *
 * `@canonicalName:CANONICAL.NAME`
 *
 * #### Example
 *
 * `@canonicalName:foo.bar.SuperPowerExtension`
 *
 * ### JavaSpecName
 *
 * #### Expression
 *
 * `@javaSpecName:Ljava/spec/Name;`
 *
 * #### Example
 *
 * `@javaSpecName:Lfoo/bar/SuperPowerExtension;`
 */
sealed class IdExpression {
    abstract fun match(type: Type): Boolean

    class RegexFind(val pattern: Regex): IdExpression() {
        override fun match(type: Type): Boolean =
            pattern.find(type.javaSpecName) != null
    }

    class RegexMatch(val pattern: Regex): IdExpression() {
        override fun match(type: Type): Boolean =
            pattern.matchEntire(type.javaSpecName) != null
    }

    class SimpleName(val name: String): IdExpression() {
        override fun match(type: Type): Boolean =
            type.simpleName == name
    }
    class CanonicalName(val name: String): IdExpression() {
        override fun match(type: Type): Boolean =
            type.canonicalName == name
    }

    class JavaSpecName(val name: String): IdExpression() {
        override fun match(type: Type): Boolean =
            type.javaSpecName == name
    }
}

fun String.parseIdExpressionOrFail(): IdExpression =
    this.parseIdExpression() ?: throw IllegalStateException("Illegal id expression: ${this}")

fun String.parseIdExpression(): IdExpression? =
    this.indexOf(':').let { com ->
        if (com != -1) this.substring(0, com) to this.substring(com + 1)
        else null
    }?.let { (prefix, postfix) ->
        when(prefix) {
            "@regexFind" -> IdExpression.RegexFind(Regex(postfix))
            "@regexMatch" -> IdExpression.RegexMatch(Regex(postfix))
            "@simpleName" -> IdExpression.SimpleName(postfix)
            "@canonicalName" -> IdExpression.CanonicalName(postfix)
            "@javaSpecName" -> IdExpression.JavaSpecName(postfix)
            else -> null
        }
    }