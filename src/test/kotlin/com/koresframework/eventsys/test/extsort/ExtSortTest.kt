/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.extsort

import com.koresframework.eventsys.extension.ExtensionOrder
import com.koresframework.eventsys.extension.ExtensionSpecification
import com.koresframework.eventsys.sorter.ExtensionSorter
import com.koresframework.kores.type.simpleName
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class ExtSortTest: FunSpec({
    test("test extension sort") {
        val first = ExtensionSpecification(
            Unit,
            null,
            A::class.java,
            ExtensionOrder(
                after = listOf(B::class.java)
            )
        )
        val second = ExtensionSpecification(
            Unit,
            null,
            B::class.java,
            ExtensionOrder(after = listOf(C::class.java))
        )

        val third = ExtensionSpecification(
            Unit,
            null,
            C::class.java,
            ExtensionOrder()
        )
        val l = listOf(first, second, third)
        val sorted = l.sortedWith(ExtensionSorter)

        l.map { it.extensionClass!!.simpleName }
            .shouldBe(listOf("A", "B", "C"))

        sorted.map { it.extensionClass!!.simpleName }
            .shouldBe(listOf("C", "B", "A"))
    }


}) {
    class A
    class B
    class C
}