/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.channel

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Filter
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.event.annotation.Name
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.util.createFactory
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ChannelTest {
    var calls = 0

    @BeforeEach
    fun setup() {
        calls = 0
    }

    @Test
    fun channel() {
        val eventManager = DefaultEventManager()
        eventManager.eventListenerRegistry.registerListeners(this, this)

        val user = User(id = 0, name = "Test", email = "test@test.com")
        val factory = eventManager.eventGenerator.createFactory<EventFactory>().resolve()

        runBlocking {
            eventManager.dispatch(factory.createUserRegisterEvent(user), this, "user")
        }

        Assertions.assertEquals(1, this.calls)

        runBlocking {
            eventManager.dispatch(factory.createUserRegisterEvent(user), this, "other")
        }

        Assertions.assertEquals(1, this.calls)
    }

    interface EventFactory {
        fun createUserRegisterEvent(@Name("user") user: User): UserRegisterEvent
    }

    @Filter(UserRegisterEvent::class)
    @Listener(channel = "user")
    fun onUserRegister(user: User) {
        calls++
    }

    interface UserRegisterEvent : Event {
        val user: User
    }
}

data class User(val id: Int, val name: String, val email: String)