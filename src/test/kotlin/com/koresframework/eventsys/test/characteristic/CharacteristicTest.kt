/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.characteristic

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.characteristic.CharacteristicHolder
import com.koresframework.eventsys.event.characteristic.CharacteristicKey
import com.koresframework.eventsys.event.characteristic.CharacteristicValue
import com.koresframework.eventsys.event.copySame
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.util.createFactory
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

interface CharacteristicFactory {
    fun createGreetEvent(name: String): GreetEvent
}

interface GreetEvent : Event, CharacteristicHolder<GreetEvent> {
    val name: String
}

object MyChar : CharacteristicKey<String> {
    override val id: String = "com.my.Char"

    data class Value(val value: String) : CharacteristicValue<MyChar, String>
}


class CharacteristicTest : FunSpec({
    test("event copy test") {
        val em = DefaultEventManager()
        val factory = em.eventGenerator.createFactory<CharacteristicFactory>().resolve()


        val event = factory.createGreetEvent("Foo")
        val newEvent = event.copySame(
            "name" to "Bar"
        )

        val eventWithCharacteristic = event.characteristic(MyChar, MyChar.Value("Hello world"))
        val char = eventWithCharacteristic.characteristics.get(MyChar)

        newEvent.name.shouldBe("Bar")
        char.shouldBe(MyChar.Value("Hello world"))
        event.characteristics.get(MyChar).shouldBe(null)
    }

})