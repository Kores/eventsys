/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.suspending

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.event.annotation.Name
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import com.koresframework.kores.type.typeOf
import kotlinx.coroutines.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import java.util.concurrent.atomic.AtomicInteger

class SuspendingTest {

    @RepeatedTest(3)
    fun suspending() {
        val em = DefaultEventManager()
        val factory = em.eventGenerator.createFactory<MyFactory>()()
        val envt = factory.createMyMessage("Test")
        em.eventListenerRegistry.registerListeners(this, MyListener())

        val r = em.dispatchAsyncBlocking(envt, typeOf<MyMessage>())
        val res = runBlocking {
            r.await()
        }

        val maximum = max.get()
        Assertions.assertEquals(2, maximum)
    }

}

val max = AtomicInteger()
val current = AtomicInteger()

fun inc() {
    current.incrementAndGet()
    max.updateAndGet {
        it.coerceAtLeast(current.get())
    }
}

fun dec() {
    current.decrementAndGet()
}

class MyListener {

    @Listener
    suspend fun onMessage(event: MyMessage): String {
        println("Event: $event")
        inc()
        coroutineScope {
            while (isActive && max.get() < 2) {
                delay(10L)
            }
        }
        dec()
        println("Event: $event -> End")
        return "a"//ListenResult.Value(Unit)
    }

    @Listener
    suspend fun onMessage2(event: MyMessage): ListenResult {
        println("Event 2: $event")
        inc()
        coroutineScope {
            while (isActive && max.get() < 2) {
                delay(10L)
            }
        }
        dec()
        println("Event 2: $event -> End")
        return ListenResult.Value(Unit)
    }

}

interface MyFactory {
    fun createMyMessage(@Name("text") text: String): MyMessage
}

interface MyMessage : Event {
    val text: String
}