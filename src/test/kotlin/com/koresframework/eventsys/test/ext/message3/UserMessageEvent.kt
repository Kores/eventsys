/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.ext.message3

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.event.annotation.Extensions
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.coroutines.awaitAll

data class User(val name: String, val role: String)

interface MessageEvent : Event {
    val role: String

    val message: String
    val completeMessage: String
        get() = "[$role] $message"
}

class UserMessageExtension<E>(private val message: E) where E: MessageEvent, E: UserEvent {
    // Default role implementation
    val role: String
        get() = this.message.user.role
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = UserMessageExtension::class)
    ])
    fun createUserMessageEvent(user: User, message: String): MessageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserMessageEvent(event: F): String where F: MessageEvent, F: UserEvent {
        println("= Message = ${event.completeMessage}")
        return event.completeMessage
    }
}

class ExtTest : FunSpec({
    test("role default impl provided by extension") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createUserMessageEvent(
            User("Foo", "user"),
            "Hello world"
        )

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        val firstResult = results.first().result
        firstResult.shouldBeInstanceOf<ListenResult.Value>()
        firstResult.value.shouldBe("[user] Hello world")

        val ext = message.getExtension(UserMessageExtension::class.java)
        ext.shouldNotBeNull()
        ext.role.shouldBe("user")
    }
})