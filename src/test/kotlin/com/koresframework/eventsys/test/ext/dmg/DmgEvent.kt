/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.ext.dmg

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.event.annotation.Extensions
import com.koresframework.eventsys.event.annotation.Filter
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.event.property.GetterProperty
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import com.koresframework.kores.type.Generic
import com.koresframework.kores.type.typeOf
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.awaitAll

enum class DamageType {
    POISON,
    SWORD
}

data class User(
    val name: String,
    val weakness: List<DamageType>
)

fun interface DamageMultiplier {
    fun multiply(damage: Double): Double
}

interface DamageEvent : Event {
    val damage: Double
    val type: DamageType
    val multipliers: MutableList<DamageMultiplier>

    val totalDamage: Double
        get() = if (this.multipliers.isEmpty()) this.damage else this.multipliers.sumOf { it.multiply(damage) }

    fun addMultiplier(multiplier: DamageMultiplier) {
        this.multipliers.add(multiplier)
    }

    fun addMultiplier(multiplier: Double) {
        this.addMultiplier {
            it * multiplier
        }
    }

}

class DamageExtension(private val damageEvent: DamageEvent) {
    init {
        val userProp = this.damageEvent.lookup(User::class.java, "user")
        if (userProp is GetterProperty<User>) {
            val user = userProp.getValue()
            user.weakness.filter { it == this.damageEvent.type }
                    .forEach { _ -> this.damageEvent.addMultiplier { it * 2 } }
        }
    }
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = DamageExtension::class)
    ])
    fun createDamageEvent(user: User, damage: Double, type: DamageType, multipliers: MutableList<DamageMultiplier> = mutableListOf()): DamageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserDamageEvent(event: F): Double where F: DamageEvent, F: UserEvent {
        println("Total damage: ${event.totalDamage}. Damage: ${event.damage}")
        return event.totalDamage
    }
}

class ExtTest : FunSpec({
    test("damage modifier extension") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createDamageEvent(
            User("admin", listOf(DamageType.POISON)),
            1.0,
            DamageType.POISON
        )

        message.addMultiplier(5.0)

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        results.first { it.result == ListenResult.Value(7.0) }
    }
})