/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.ext

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.event.annotation.Filter
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import com.koresframework.kores.type.Generic
import com.koresframework.kores.type.typeOf
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.awaitAll

interface MessageEvent : Event {
    val message: String
}

interface UserEvent : Event {
    val userLogin: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent

    @Extension(implement = UserEvent::class)
    fun createUserMessageEvent(userLogin: String, message: String): MessageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserMessageEvent(event: F) where F: MessageEvent, F: UserEvent {
        println("Event: ${event}")
    }

    @Listener
    @Filter
    suspend fun onUserMessageEvent(userLogin: String, message: String) {
        println("Login: $userLogin: $message")
    }
}

class ExtTest : FunSpec({
    test("ext") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createUserMessageEvent("admin", "Hello world!")

        message.getGetterProperty(String::class.java, "userLogin")!!.getValue()
            .shouldBe("admin")

        message.message.shouldBe("Hello world!")

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        results.first { it.result == ListenResult.Value(Unit) }
    }
})