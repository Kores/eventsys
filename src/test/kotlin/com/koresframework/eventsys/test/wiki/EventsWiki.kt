/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.wiki

import com.github.jonathanxd.iutils.type.TypeInfo
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.property.GetterProperty
import com.koresframework.eventsys.event.property.Property
import com.koresframework.eventsys.event.property.primitive.IntGetterProperty
import com.koresframework.eventsys.gen.event.EventClassSpecification
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.typed.typedGet
import com.koresframework.eventsys.result.success
import com.koresframework.eventsys.util.EventListener
import com.koresframework.eventsys.util.create
import com.koresframework.eventsys.util.createEventClass
import com.koresframework.eventsys.util.createFactory
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*
import java.util.function.IntSupplier
import java.util.function.Supplier

class EventsWiki {
    @Test
    fun eventsWiki() {
        val manager = DefaultEventManager()

/*
        manager.eventGenerator.registerExtension(TransactionEvent::class.java, ExtensionSpecification(
                residence = Unit,
                extensionClass = ApplyExt::class.java,
                implement = null
        ))
*/
        //manager.eventGenerator.options[EventGeneratorOptions.ASYNC] = true
        val factory = manager.eventGenerator.createFactory<MyFactory>()()
        val eventClass = manager.eventGenerator.createEventClass<BuyEvent>()()

        val event = create(eventClass, mapOf(
                "product" to Product("USB Adapter", 10.0),
                "amount" to 5
        ))

        runBlocking {
            manager.dispatch(event, this)
        }

        manager.eventListenerRegistry.registerListener<BuyEvent>(this, BuyEvent::class.java, EventListener { theEvent, _ ->
            Assertions.assertEquals("USB Adapter", theEvent.product.name)
            Assertions.assertEquals(10.0, theEvent.product.price, 0.0)
            Assertions.assertEquals(5, theEvent.amount)
            return@EventListener success()
        })

        val bus = Business("x")
        val c = factory.createBuyEvent(Product("USB Adapter", 10.0), 5, bus)

        Assertions.assertEquals(bus, c.getGetterProperty(Business::class.java, "business")?.getValue())

        val evt = factory.createBuyEvent2(Product("USB Adapter", 10.0))

        Assertions.assertEquals(10, evt.amount)

        val transac = factory.createTransactionEvent(5.0)

        transac.apply { it * it }

        Assertions.assertEquals(5.0 * 5.0, transac.amount, 0.0)
    }

    @Test
    fun staticEventsWiki() {
        val manager = DefaultEventManager()

        manager.eventGenerator.registerEventImplementation<BuyEvent>(
                EventClassSpecification(TypeInfo.of(BuyEvent::class.java), emptyList(), emptyList()),
                BuyEventImpl::class.java
        )
    }

    data class BuyEventImpl(override val product: Product, override val amount: Int) : BuyEvent {
        private val properties = Collections.unmodifiableMap(mapOf(
                "product" to GetterProperty.Impl(Product::class.java, Supplier { this.product }),
                "amount" to IntGetterProperty.Impl(IntSupplier { this.amount })
        ))

        override fun getProperties(): Map<String, Property<*>> =
                this.properties

        override fun copy(newProperties: Map<String, Any>): Event =
            this.copy(
                product = newProperties.typedGet("product") ?: this.product,
                amount = newProperties.typedGet("amount") ?: this.amount
            )
    }
}