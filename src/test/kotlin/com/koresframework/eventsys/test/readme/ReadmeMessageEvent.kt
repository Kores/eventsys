/*
 *      EventSys - Event implementation generator written on top of Kores
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 ProjectSandstone <https://github.com/ProjectSandstone/EventSys>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.eventsys.test.readme

import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Listener
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

interface ReadmeMessageEvent : Event {
    val message: String
}

interface ReadmeFactory {
    fun createMessageEvent(message: String): ReadmeMessageEvent
}

class MessageListener {
    @Listener
    fun onMessage(message: ReadmeMessageEvent): String {
        return message.message
    }
}

class ReadmeTest : FunSpec({
    test("readme") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<ReadmeFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MessageListener())

        val message = factory.createMessageEvent("Hello world!")
        message.message.shouldBe("Hello world!")

        val result = manager.dispatch(message, this)

        result.listenExecutionResults.first().await()
            .result.shouldBe(ListenResult.Value("Hello world!"))
    }
})