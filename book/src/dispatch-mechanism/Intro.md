# Introduction to dispatch mechanism

## A bit of history

**EventSys** is an event system based mainly on **code generation**, it was designed to be used in games, which relies on
events to propagate changes and interactions. 

Using Dynamic Code Generation allowed **EventSys** to be able to provide
a ground for events that can happen with different kind of entities and in different contexts. 
With **EventSys**, instead of having a generic `DamageEvent` and different inheritors, like `MonsterDamageEvent`,
`PlayerDamageEvent`, `PetDamageEvent`, you just provide a single `DamageEvent` and different “Traits”, 
like `MonsterEvent`, `PlayerEvent`, `PetEvent`, those “Traits” can be implemented by any event.

So, initially, EventSys dispatch mechanism was based on *Event Types* and *Properties*, in which interfaces
the generated event class implements and which properties the event holds.

However, EventSys proved to be a suitable alternative to *Microservices Messaging* when those messages were mere Events.

### Here comes EventSys 2.0

EventSys 2.0 roadmap focused on Event Channels, a way of dispatching events to different groups of listeners, providing
the initial ground for **Atevist**, a scalable listener orchestrator written on top of EventSys.