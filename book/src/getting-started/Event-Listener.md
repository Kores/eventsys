# Event Listener

In order to “catch” an Event, you need an Event Listener. There is two types of Event Listeners, those which implements
`EventListener` interface and those which are just functions annotated with `@Listener`.

## `EventListener` implementation

```kotlin
class MessageEventListener : EventListener<MessageEvent> {
    override suspend fun onEvent(event: MessageEvent, dispatcher: Any): ListenResult {
        println("Received message: ${event.message}")
        return ListenResult.Value(Unit)
    }
}
```

## Annotated Listeners

```kotlin
class MyListener {
    @Listener
    fun onMessageEvent(event: MessageEvent) {
        println("Received message: ${event.message}")
    }
}
```

### Suspend Listener


```kotlin
class MyListener {
    @Listener
    suspend fun onMessageEvent(event: MessageEvent) {
        println("Received message: ${event.message}")
    }
}
```


## Performance

Both approaches do have the same overall performance, however, annotated listeners depends on
a dynamic generation of a `EventListener` implementation, impacting the performance at registration-time 
and depends on further optimizations that will be made by the JVM JIT to reach the peak performance.

You don't need to worry about this performance impact of code generation, as explained [here](../Code-Generation.md).