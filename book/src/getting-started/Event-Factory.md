# Event Factory

Event Factory is the way to create events without interacting with the dynamic code generation mechanism.

```kotlin
interface MyEventFactory {
    fun createMessageEvent(message: String): MessageEvent
}
```

# Dive into generated code

You can enable the save of generated classe by providing `kores.generation.save=true` property at runtime.

Below is an example of a generated factory interface implementation:

```java
public class ReadmeFactory$Impl implements ReadmeFactory {
    private final EventGenerator eventGenerator;

    public ReadmeFactory$Impl(EventGenerator eventGenerator) {
        this.eventGenerator = eventGenerator;
    }

    public ReadmeMessageEvent createMessageEvent(String message) {
        return new ReadmeMessageEventImpl(message);
    }
}
```