# Getting Started

## Add dependencies

First add the EventSys dependency to your project

Gradle (Kotlin DSL):

```gradle
repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/29384742/packages/maven")
}

dependencies {
    implementation("com.github.koresframework:eventsys:2.0.0-rc3")
}
```

## Declare your events, factories and listeners

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent
}

class MessageEventListener {
    @Listener
    suspend fun onMessageEvent(evente: MessageEvent) {
        println("Received message: ${event.message}")
    }
}
```

## Create a Manager, register listeners and retrieve factory instance

```kotlin
class EventService(val eventManager: EventManager = DefaultEventManager()) {
    val factory = eventManager.eventGenerator.createFactory<EventFactory>().resolve()
    
    init {
        eventManager.eventListenerRegistry.registerListeners(this, MessageEventListener())
    }
}
```

## Create event and dispatch

```kotlin
class EventService(val eventManager: EventManager = DefaultEventManager()) {
    val factory = eventManager.eventGenerator.createFactory<EventFactory>().resolve()
    
    init {
        eventManager.eventListenerRegistry.registerListeners(this, MessageEventListener())
    }
    
    suspend fun sendMessage(message: String) {
        val messageEvent = factory.createMessageEvent("Hello world!")
        val result = eventManager.dispatch(messageEvent, this)
    }
}
```
