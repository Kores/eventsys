# Event Extensions - Generics

Generic types provided to **implementation extension** are mixed with the generic types of the event itself, for example:

```kotlin
interface MessageEvent<M> : Event {
    val message: M
}

interface MyEventFactory {
    fun <M> createMessageEvent(@TypeParam type: Type, message: M): MessageEvent<M>
}
```

The type of `message` property depends on the type of the `MessageEvent` instance. EventSys supports both erasure and
reification, the latter one being more rare because the event type can be supplied as an argument to the event instance, thus allowing
event to preserve their types.

In this scenario, if you try to add an extension like this:

```kotlin
class MyExt(val event: Event) {
    val message: String get() = "Hello world"
}
```

And add the extension annotation:

```kotlin
@Extension(extensionClass = MyExt::class)
interface MessageEvent<M> : Event {
    val message: M
}

interface MyEventFactory {
    fun <M> createMessageEvent(@TypeParam type: Type, message: M): MessageEvent<M>
}
```

Then creating the event:
```kotlin
val em = DefaultEventManager()
val factory = em.eventGenerator.createFactory<MyEventFactory>().resolve()
val dt = factory.createMessageEvent(genericTypeOf<MessageEvent<String>>(), "Test")
println(dt.message)
```

This will print “Test” and not “Hello world”.

Why this happens?

`dt.message` is compiled to `invokeVirtual Lmy/pkg/MessageEvent; getMessage()Ljava/lang/Object;`, which resolves to the
function defined in `MessageEvent` instead of the one defined in `MyExt`, this is because both Java and Kotlin 
applies type erasure.

EventSys, in turn, generates both `getMessage()Ljava/lang/Object;`, which is the property defined in `MessageEvent` and
`getMessage()Ljava/lang/String;` which is defined in `MyExt`, and it is unable to determine that the `getMessage` defined
in `MyExt` overrides the one defined in `MessageEvent<M>` because the types are incompatible, and
it would need a runtime validation to select the correct implementation, which EventSys does not do because of the complexity
it introduces in events.

The right way to solve this is to be generic on the event implementation as well, by **matching the type arguments**:

```kotlin
class MyExt<M>(val event: Event) {
    val message: M get() = TODO()
}
```

By **matching the type arguments** we mean that your event extension type arguments must have the same name as the event
type arguments, so if the event is `MyEvent<A>` then your extension must be `MyExt<A>`.
If it does not match the criteria,
the mixed functions will not inherit the same type as the event, so they will have a unique identity. There are some scenario
that this is the desired and adequate behavior, like in [`CharacteristicExt`](https://gitlab.com/Kores/eventsys/-/blob/3fdf3d51d39c00f6939298e06c751eba1358eb3d/src/main/kotlin/com/koresframework/eventsys/event/characteristic/CharacteristicExt.kt).