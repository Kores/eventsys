# Event Interface

Events are represented by an Event Interface, which holds a set of Properties (which are Serializable), for example:

```kotlin
interface MessageEvent : Event {
    val message: String
}
```

This interface represents an Event that has a `message` property of type `String`.

## Event Implementation

Event implementation is generated automatically, you don't need to care about the event implementation, all implementations
follows a pattern that is very important, since EventSys is channel and property oriented, instead of Event-type oriented.
This means that it does not matter the type of the event, but the properties it carries and the channel it is dispatched to.

Even though Event Type is not that important for the event dispatch mechanism, it is important from the point of view of
the development, since it is used to determine the **kind** of the event.

## Properties

Properties are values carried by the event instance. In **EventSys**, properties are indexed into an immutable **HashMap**
and can be queried through `PropertyHolder` interface function. This means that querying for properties by name is a `O(1)` operation.

Property-system allows **EventSys** to care more about the properties an event carries than the type of the event, which
is an interesting assumption, given the ability of **EventSys** to generate event implementations dynamically.

Also, properties allows [**event destruction**](Event-Listener.md) to happen in [event listeners](Event-Listener.md)
and [event extensions](Event-Extension.md) to be provided, which causes a generation of a different event implementation
that can carry more properties and have default implementation of event functions.


# Dive into generated code

You can enable the save of generated classe by providing `kores.generation.save=true` property at runtime.

```java
public class ReadmeMessageEventImpl implements ReadmeMessageEvent {
    private final String message;
    private final Map<String, Property<?>> _properties = new HashMap();
    private final Map<String, Property<?>> _immutable_properties;

    public ReadmeMessageEventImpl(@Name("message") String message) {
        this._immutable_properties = Collections.unmodifiableMap(this._properties);
        this.message = message;
        this._properties.put("message", new Impl(String.class, this::getMessage));
    }

    public String getMessage() {
        return this.message;
    }

    public Type getEventType() {
        return (Type)KoresTypes.getKoresType(ReadmeMessageEvent.class);
    }

    public String toString() {
        String var10000 = this.getClass().getSimpleName();
        return "{class=" + var10000 + ",type=" + this.getEventType().toString() + ",properties=[message],extensions=[]}";
    }

    public <T> T getExtension(Class<T> extensionClass) {
        return (Object)null;
    }

    @Override
    public Map<String, Property<?>> getProperties() {
        return this._immutable_properties;
    }
}
```