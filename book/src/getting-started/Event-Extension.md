# Event Extensions

EventSys allows extension of events through **Event Extensions** mechanism which acts like a **Trait system**, allowing
additional properties and functions to be provided.

You can install **extensions** using factory functions:

```kotlin
interface MessageEvent : Event {
    val message: String
}

interface UserEvent : Event {
    val userLogin: String
}

interface EventFactory {
    fun createMessageEvent(message: String): MessageEvent

    @Extension(implement = UserEvent::class)
    fun createUserMessageEvent(userLogin: String, message: String): MessageEvent
}
```

And create them as regular events:

```kotlin
val manager = DefaultEventManager()
val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()

val message = factory.createUserMessageEvent("admin", "Hello world!")
```

## Listener

It is possible to create a `listener function` which handles events of more than one type, by using **Intersection Types**:

```kotlin
class MyListener {
    @Listener
    suspend fun <E> onUserMessageEvent(event: E) where E: MessageEvent, E: UserEvent {
        println("OnEvent: $event")
    }
}
```

However, there is another convenient way of handling events that does have more than one type, by using destruction:

```kotlin
class MyListener {
    @Listener
    @Filter
    suspend fun onUserMessageEvent(userLogin: String, message: String) {
        println("$userLogin: $message")
    }
}
```

But be aware that listeners that use `destruction` accepts all events, unless you specify allowed event types in the `@Filter`
annotation or as the first argument of the function.

## Implementation extension

You can also provide default implementation for events and extensions functions by using the **Implementation extension**. 
The code below shows an example of an **implementation extension** that adds a default multiplier for *User weakness* 
when `user` property is present:

```kotlin
enum class DamageType {
    POISON,
    SWORD
}

data class User(
    val name: String,
    val weakness: List<DamageType>
)

fun interface DamageMultiplier {
    fun multiply(damage: Double): Double
}

interface DamageEvent : Event {
    val damage: Double
    val type: DamageType
    val multipliers: MutableList<DamageMultiplier>

    val totalDamage: Double
        get() = if (this.multipliers.isEmpty()) this.damage else this.multipliers.sumOf { it.multiply(damage) }

    fun addMultiplier(multiplier: DamageMultiplier) {
        this.multipliers.add(multiplier)
    }

    fun addMultiplier(multiplier: Double) {
        this.addMultiplier {
            it * multiplier
        }
    }

}

class DamageExtension(private val damageEvent: DamageEvent) {
    init {
        val userProp = this.damageEvent.lookup(User::class.java, "user")
        if (userProp is GetterProperty<User>) {
            val user = userProp.getValue()
            user.weakness.filter { it == this.damageEvent.type }
                    .forEach { _ -> this.damageEvent.addMultiplier { it * 2 } }
        }
    }
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = DamageExtension::class)
    ])
    fun createDamageEvent(user: User, damage: Double, type: DamageType, multipliers: MutableList<DamageMultiplier> = mutableListOf()): DamageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserDamageEvent(event: F): Double where F: DamageEvent, F: UserEvent {
        println("Total damage: ${event.totalDamage}. Damage: ${event.damage}")
        return event.totalDamage
    }
}

class ExtTest : FunSpec({
    test("extension providing additional multipliers") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createDamageEvent(
            User("admin", listOf(DamageType.POISON)),
            1.0,
            DamageType.POISON
        )

        message.addMultiplier(5.0)

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        results.first { it.result == ListenResult.Value(7.0) }
    }
})
```

This is useful for cases where the **Event** is provided by an external module and custom implementations are provided
by platform-specific code or the implementation can be customized externally.

### Function override

**Implementation extensions** are able to override functions that already has a default implementation, for example:

```kotlin
data class User(val name: String, val role: String)

interface MessageEvent : Event {
    val role: String

    val message: String
    val completeMessage: String
        get() = "[$role] $message"
}

class MessageExtension(private val message: MessageEvent) {
    val completeMessage: String
        get() = "[*] [${this.message.role}] ${this.message.message}"
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = MessageExtension::class)
    ])
    fun createUserMessageEvent(user: User, message: String, role: String = user.role): MessageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserMessageEvent(event: F): String where F: MessageEvent, F: UserEvent {
        println("= Message = ${event.completeMessage}")
        return event.completeMessage
    }
}

class ExtTest : FunSpec({
    test("property override by extension") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createUserMessageEvent(
            User("Foo", "admin"),
            "Hello world"
        )

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        val firstResult = results.first().result
        firstResult.shouldBeInstanceOf<ListenResult.Value>()
        firstResult.value.shouldBe("[*] [admin] Hello world")
    }
})
```

In this example, the extension override the `completeMessage` property, but you need to be careful when doing this,
**implementation extension** receives the event itself as instance, this means that it is not possible to invoke the original
implementation of the override function. So the following code is not valid:

```kotlin
class MessageExtension(private val message: MessageEvent) {
    val completeMessage: String
        get() = "[*] ${this.message.completeMessage}" // Throws StackOverflowError
}
```

But there is a workaround, if you need to invoke the default implementation you can use `MethodHandles` API:

```kotlin
class MessageExtension(private val message: MessageEvent) {
    private val lookup = MethodHandles.lookup()
    private val getCompleteMessage = lookup
        .findSpecial(MessageEvent::class.java, "getCompleteMessage", MethodType.methodType(String::class.java), message::class.java)
        .bindTo(message)

    val completeMessage: String
        get() = "[*] ${this.getCompleteMessage.invokeExact() as String}"
}
```

### Implement properties

You can also use **implementation extension** to provide a default implementation for properties in cases that you know
where their value came from:

```kotlin
data class User(val name: String, val role: String)

interface MessageEvent : Event {
    val role: String

    val message: String
    val completeMessage: String
        get() = "[$role] $message"
}

class UserMessageExtension<E>(private val message: E) where E: MessageEvent, E: UserEvent {
    // Default role implementation
    val role: String
        get() = this.message.user.role
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = UserMessageExtension::class)
    ])
    fun createUserMessageEvent(user: User, message: String): MessageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserMessageEvent(event: F): String where F: MessageEvent, F: UserEvent {
        println("= Message = ${event.completeMessage}")
        return event.completeMessage
    }
}

class ExtTest : FunSpec({
    test("property override by extension") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createUserMessageEvent(
            User("Foo", "user"),
            "Hello world"
        )

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        val firstResult = results.first().result
        firstResult.shouldBeInstanceOf<ListenResult.Value>()
        firstResult.value.shouldBe("[user] Hello world")
    }
})
```

### Implementation override works for `suspend fun` as well

If your event does have a `suspend` function, providing implementation in an **implementation extension** works as well:

```kotlin
data class User(val name: String, val role: String)

interface MessageEvent : Event {
    val provider: suspend () -> String

    suspend fun message(): String
}

class UserMessageExtension<E>(private val message: E) where E: MessageEvent, E: UserEvent {
    // Default role implementation
    val role: String
        get() = this.message.user.role

    suspend fun message(): String = message.provider()
}

interface UserEvent : Event {
    val user: User
}

interface EventFactory {
    @Extensions(value = [
        Extension(implement = UserEvent::class),
        Extension(extensionClass = UserMessageExtension::class)
    ])
    fun createUserMessageEvent(user: User, provider: suspend () -> String): MessageEvent
}

class MyListener {
    @Listener
    suspend fun <F> onUserMessageEvent(event: F): String where F: MessageEvent, F: UserEvent {
        val message = event.message()
        println("= Message = $message")
        return message
    }
}

class ExtTest : FunSpec({
    test("suspend fun provided by extension") {
        val manager = DefaultEventManager()
        val factory = manager.eventGenerator.createFactory<EventFactory>().resolve()
        manager.eventListenerRegistry.registerListeners(this, MyListener())

        val message = factory.createUserMessageEvent(
            User("Foo", "user")
        ) { "Hello world" }

        val result = manager.dispatch(message, this)

        val results = result.listenExecutionResults.awaitAll()

        val firstResult = results.first().result
        firstResult.shouldBeInstanceOf<ListenResult.Value>()
        firstResult.value.shouldBe("Hello world")

        val ext = message.getExtension(UserMessageExtension::class.java)
        ext.shouldNotBeNull()
        ext.role.shouldBe("user")
    }
})
```

But be careful, the `suspend` override function does receive the same `Continuation` provided to the original function,
this is the same behavior as [suspend listener](Event-Listener.md#suspend-listener). EventSys does not generate
additional **state-machine** code for `suspend`, it only does regular delegation, which is enough.

## Events does not implement **implementation extension**

Even though functions and properties of **implementation extensions** are mirrored in the generated class,
the class does not implement the provided class. The generated class only implements the interface
provided to the **implement** property of `@Extension`.

In order to retrieve the instance of a **extensionClass** you need to use the **getExtension** function:

```kotlin
val ext = message.getExtension(UserMessageExtension::class.java)
```

# Dive in generated code

You can enable the save of generated classe by providing `kores.generation.save=true` property at runtime.

The following event implementation will be generated for the last example:

```java
public class MessageEventImpl implements MessageEvent, UserEvent, ExtensionHolder {
    private final String message;
    private final User user;
    private final Map<String, Property<?>> _properties = new HashMap();
    private final Map<String, Property<?>> _immutable_properties;
    private final UserMessageExtension extension_UserMessageExtension;

    public MessageEventImpl(@Name("message") String message, @Name("user") User user) {
        this._immutable_properties = Collections.unmodifiableMap(this._properties);
        this.message = message;
        this.user = user;
        this._properties.put("message", new Impl(String.class, this::getMessage));
        this._properties.put("user", new Impl(User.class, this::getUser));
        this.extension_UserMessageExtension = new UserMessageExtension(this);
    }

    public String getMessage() {
        return this.message;
    }

    public User getUser() {
        return this.user;
    }

    public Type getEventType() {
        return (Type)Generic.type("E").of(new Bound[]{new Extends(KoresTypes.getKoresType(MessageEvent.class)), new Extends(KoresTypes.getKoresType(UserEvent.class))});
    }

    public String toString() {
        String var10000 = this.getClass().getSimpleName();
        return "{class=" + var10000 + ",type=" + this.getEventType().toString() + ",properties=[message, user],extensions=[[impl=UserEvent,ext=null,residence=method(MessageEvent createUserMessageEvent(User, String))], [impl=null,ext=UserMessageExtension,residence=method(MessageEvent createUserMessageEvent(User, String))]]}";
    }

    public String getRole() {
        return this.extension_UserMessageExtension.getRole();
    }

    public <T> T getExtension(Class<T> extensionClass) {
        switch(extensionClass.getCanonicalName().hashCode()) {
        case -1682303624:
            if (extensionClass.getCanonicalName().equals("com.koresframework.eventsys.test.ext.message3.UserMessageExtension")) {
                return (Object)this.extension_UserMessageExtension;
            }
        default:
            return (Object)null;
        }
    }

    @Override
    public Map<String, Property<?>> getProperties() {
        return this._immutable_properties;
    }
}
```

```java
public class MessageEventImpl implements MessageEvent, UserEvent, ExtensionHolder {
    private final String message;
    private final String role;
    private final User user;
    private final Map<String, Property<?>> _properties = new HashMap();
    private final Map<String, Property<?>> _immutable_properties;
    private final MessageExtension extension_MessageExtension;

    public MessageEventImpl(@Name("message") String message, @Name("role") String role, @Name("user") User user) {
        this._immutable_properties = Collections.unmodifiableMap(this._properties);
        this.message = message;
        this.role = role;
        this.user = user;
        this._properties.put("message", new Impl(String.class, this::getMessage));
        this._properties.put("role", new Impl(String.class, this::getRole));
        this._properties.put("user", new Impl(User.class, this::getUser));
        this.extension_MessageExtension = new MessageExtension(this);
    }

    public String getMessage() {
        return this.message;
    }

    public String getRole() {
        return this.role;
    }

    public User getUser() {
        return this.user;
    }

    public Type getEventType() {
        return (Type)Generic.type("E").of(new Bound[]{new Extends(KoresTypes.getKoresType(MessageEvent.class)), new Extends(KoresTypes.getKoresType(UserEvent.class))});
    }

    public String toString() {
        String var10000 = this.getClass().getSimpleName();
        return "{class=" + var10000 + ",type=" + this.getEventType().toString() + ",properties=[message, role, user],extensions=[[impl=UserEvent,ext=null,residence=method(MessageEvent createUserMessageEvent(User, String, String))], [impl=null,ext=MessageExtension,residence=method(MessageEvent createUserMessageEvent(User, String, String))]]}";
    }

    public String getCompleteMessage() {
        return this.extension_MessageExtension.getCompleteMessage();
    }

    public <T> T getExtension(Class<T> extensionClass) {
        switch(extensionClass.getCanonicalName().hashCode()) {
        case 1692828325:
            if (extensionClass.getCanonicalName().equals("com.koresframework.eventsys.test.ext.dmg2.MessageExtension")) {
                return (Object)this.extension_MessageExtension;
            }
        default:
            return (Object)null;
        }
    }

    @Override
    public Map<String, Property<?>> getProperties() {
        return this._immutable_properties;
    }
}
```

```java
public class MessageEventImpl implements MessageEvent, UserEvent, ExtensionHolder {
    private final Function1 provider;
    private final User user;
    private final Map<String, Property<?>> _properties = new HashMap();
    private final Map<String, Property<?>> _immutable_properties;
    private final UserMessageExtension extension_UserMessageExtension;

    public MessageEventImpl(@Name("provider") Function1<Continuation<? super String>> provider, @Name("user") User user) {
        this._immutable_properties = Collections.unmodifiableMap(this._properties);
        this.provider = provider;
        this.user = user;
        this._properties.put("provider", new Impl(Function1.class, this::getProvider));
        this._properties.put("user", new Impl(User.class, this::getUser));
        this.extension_UserMessageExtension = new UserMessageExtension(this);
    }

    public Function1<Continuation<? super String>> getProvider() {
        return (Function1)this.provider;
    }

    public User getUser() {
        return this.user;
    }

    public Type getEventType() {
        return (Type)Generic.type("E").of(new Bound[]{new Extends(KoresTypes.getKoresType(MessageEvent.class)), new Extends(KoresTypes.getKoresType(UserEvent.class))});
    }

    public String toString() {
        String var10000 = this.getClass().getSimpleName();
        return "{class=" + var10000 + ",type=" + this.getEventType().toString() + ",properties=[provider, user],extensions=[[impl=UserEvent,ext=null,residence=method(MessageEvent createUserMessageEvent(User, Function1))], [impl=null,ext=UserMessageExtension,residence=method(MessageEvent createUserMessageEvent(User, Function1))]]}";
    }

    public Object message(Continuation<? super String> arg0) {
        return this.extension_UserMessageExtension.message(arg0);
    }

    public String getRole() {
        return this.extension_UserMessageExtension.getRole();
    }

    public <T> T getExtension(Class<T> extensionClass) {
        switch(extensionClass.getCanonicalName().hashCode()) {
        case 950860463:
            if (extensionClass.getCanonicalName().equals("com.koresframework.eventsys.test.ext.suspend1.UserMessageExtension")) {
                return (Object)this.extension_UserMessageExtension;
            }
        default:
            return (Object)null;
        }
    }

    @Override
    public Map<String, Property<?>> getProperties() {
        return this._immutable_properties;
    }
}
```