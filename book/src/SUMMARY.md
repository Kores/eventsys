# EventSys
- [What is](What-is.md)
- [Getting Started](getting-started/Getting-Started.md)
  - [First Event Interface](getting-started/Event-Interface.md)
  - [Event Factory](getting-started/Event-Factory.md)
  - [Event Listener](getting-started/Event-Listener.md)
  - [Event Extension](getting-started/Event-Extension.md)
  - [Event Extension - Generics](getting-started/Event-Extension-Generic.md)
- [Code Generation](Code-Generation.md)