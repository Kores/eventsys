# Code Generation

EventSys uses dynamic code generation to implement [Event Interfaces](getting-started/Event-Interface.md), 
[Event Listeners](getting-started/Event-Listener.md) and [Event Factories](getting-started/Event-Factory.md). 
This allows events to be defined with simple interfaces, extended with [Event Extensions](getting-started/Event-Extension.md) and
decomposed using [Properties](getting-started/Event-Interface.md#properties).

## Performance

You don't need to care about the performance of code generation, since they happen only once and are cached for the rest
of the application execution, even for [Lazy Generated Events](getting-started/Lazy-Event-Generation.md), and the startup
impact of code generation is very little compared to the overhead that other frameworks introduces.

Dynamic code generation is done to optimize [Event Listeners](getting-started/Event-Listener.md#annotated-listeners) overall performance
and to reduce the runtime impact of reflection as well, since dynamic code generates static invocations.

Also, after JIT optimization, code generation overhead become irrelevant, even for code which generates a bunch of different
events and registres new listeners frequently.