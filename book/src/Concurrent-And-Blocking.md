# Concurrent and Blocking

Events can be dispatched in a sequential-mode or concurrent-mode. In the sequential-mode, Listeners are invoked
in order, one after another. In concurrent-mode, Listeners are detected in-order but invoked concurrently.

To dispatch in sequential-mode, just invoke the regular `dispatch` function. And to dispatch in concurrent-mode,
invoke the `dispatchAsync` variant.

## CoroutineContext

**EventSys** uses the `CoroutineContext` provided to `DefaultEventManager` constructor to dispatch events concurrently, 
if no one is provided, the default one is used, which is an `ExecutorService` with the number of threads the same
as the number of available cores multiplied by 2 (but at least 2).