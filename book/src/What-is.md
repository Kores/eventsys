# What is

EventSys is an event system for Event-Driven Development and Event based applications. It is built to facilitate creation
of events, as well as the dispatch and listen functionalities.

## EventSys vs Message Brokers

EventSys is not a message broker, message brokers are designed to distribute messages between clients,
through different queues and with different concepts in mind.

EventSys is an event system for event propagation inside the same application domain, or through network (using **Atevist**),
but it does not have Queues, Event Storage (in database) or anything like that.

In terms of scalability, EventSys uses **Channel Based Listeners**, which allows **atevist replicas** to only handle
a subset of channels, instead of handling all channels.